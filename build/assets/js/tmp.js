

ymaps.ready(['Map', 'Polygon']).then(function(){
    let map = new ymaps.Map('map', {
        center: [55.7721,37.6202],
        zoom: 14,
        controls: []
    });

    let userLayout_1 = ymaps.templateLayoutFactory.createClass('<div class="point">' +
        '<div class="point__avatar"><img src="assets/img/content/user_01.png" class="img-fluid" alt=""></div>' +
        '<div class="point__content">' +
        '<div class="point__name">Искаков А.Н.</div>' +
        '</div>' +
        '</div>'
    );

    let user_1 = new ymaps.Placemark(
        [55.7615,37.6204], {
            hintContent: 'Искаков А.Н.'
        }, {
            iconLayout: userLayout_1,
            iconShape: {
                type: 'Rectangle',
                coordinates: [
                    [-25, -25], [25, 25]
                ]
            }
        }
    );

    let userLayout_2 = ymaps.templateLayoutFactory.createClass('<div class="point">' +
        '<div class="point__avatar"><img src="assets/img/content/user_02.png" class="img-fluid" alt=""></div>' +
        '<div class="point__content">' +
        '<div class="point__name">Турсунханов Н.В.</div>' +
        '</div>' +
        '</div>'
    );

    let user_2 = new ymaps.Placemark(
        [55.7660,37.6051], {
            hintContent: 'Турсунханов Н.В.'
        }, {
            iconLayout: userLayout_2,
            iconShape: {
                type: 'Rectangle',
                coordinates: [
                    [-25, -25], [25, 25]
                ]
            }
        }
    );

    let userLayout_3 = ymaps.templateLayoutFactory.createClass('<div class="point">' +
        '<div class="point__avatar"><img src="assets/img/content/user_03.png" class="img-fluid" alt=""></div>' +
        '<div class="point__content">' +
        '<div class="point__name">Оспанов Б.Ш.</div>' +
        '<div class="point__icon"><i class="bx bx-lock-alt"></i></div>' +
        '</div>' +
        '</div>'
    );

    let user_3 = new ymaps.Placemark(
        [55.7689,37.6288], {
            hintContent: 'Оспанов Б.Ш.'
        }, {
            iconLayout: userLayout_3,
            iconShape: {
                type: 'Rectangle',
                coordinates: [
                    [-25, -25], [25, 25]
                ]
            }
        }
    );

    let userLayout_4 = ymaps.templateLayoutFactory.createClass('<div class="point point--red">' +
        '<div class="point__avatar"><img src="assets/img/content/user_04.png" class="img-fluid" alt=""></div>' +
        '<div class="point__content">' +
        '<div class="point__name">Алиев С.Г.</div>' +
        '<div class="point__icon"><span class="d-inline-block">!</span></div>' +
        '</div>' +
        '</div>'
    );

    let user_4 = new ymaps.Placemark(
        [55.7755,37.6221], {
            hintContent: 'Алиев С.Г.'
        }, {
            iconLayout: userLayout_4,
            iconShape: {
                type: 'Rectangle',
                coordinates: [
                    [-25, -25], [25, 25]
                ]
            }
        }
    );

    let userLayout_5 = ymaps.templateLayoutFactory.createClass('<div class="point point--red point--selected">' +
        '<div class="point__avatar"><img src="assets/img/content/user_05.png" class="img-fluid" alt=""></div>' +
        '<div class="point__content">' +
        '<div class="point__name">Ахметов С.Г.</div>' +
        '<div class="point__icon"><span>!</span></div>' +
        '</div>' +
        '</div>'
    );

    let user_5 = new ymaps.Placemark(
        [55.7779,37.6026], {
            hintContent: 'Ахметов С.Г.'
        }, {
            iconLayout: userLayout_5,
            iconShape: {
                type: 'Rectangle',
                coordinates: [
                    [-25, -25], [25, 25]
                ]
            }
        }
    );

    let polygon = null;
    let drawButton = document.querySelector('#draw');
    drawButton.onclick = function() {
    drawButton.disabled = true;

    drawLineOverMap(map)
    .then(function(coordinates) {
    // Переводим координаты из 0..1 в географические.
    var bounds = map.getBounds();
    coordinates = coordinates.map(function(x) {
    return [
    // Широта (latitude).
    // Y переворачивается, т.к. на canvas'е он направлен вниз.
    bounds[0][0] + (1 - x[1]) * (bounds[1][0] - bounds[0][0]),
    // Долгота (longitude).
    bounds[0][1] + x[0] * (bounds[1][1] - bounds[0][1]),
    ];
});

    // Тут надо симплифицировать линию.

    coordinates = coordinates.filter(function(_, index) {
    return index % 3 === 0;
});

    // Удаляем старый полигон.
    if (polygon) {
    map.geoObjects.remove(polygon);
}

    // Создаем новый полигон
    polygon = new ymaps.Polygon([coordinates], {}, polygonOptions);
    map.geoObjects.add(polygon);

    drawButton.disabled = false;
});
};

    map.geoObjects.add(user_1);
    map.geoObjects.add(user_2);
    map.geoObjects.add(user_3);
    map.geoObjects.add(user_4);
    map.geoObjects.add(user_5);
});

    function drawLineOverMap(map) {
    var canvas = document.querySelector('#draw-canvas');
    var ctx2d = canvas.getContext('2d');
    var drawing = false;
    var coordinates = [];

    // Задаем размеры канвасу как у карты.
    var rect = map.container.getParentElement().getBoundingClientRect();
    canvas.style.width = rect.width + 'px';
    canvas.style.height = rect.height + 'px';
    canvas.width = rect.width;
    canvas.height = rect.height;

    // Применяем стили.
    ctx2d.strokeStyle = canvasOptions.strokeStyle;
    ctx2d.lineWidth = canvasOptions.lineWidth;
    canvas.style.opacity = canvasOptions.opacity;

    ctx2d.clearRect(0, 0, canvas.width, canvas.height);

    // Показываем канвас. Он будет сверху карты из-за position: absolute.
    canvas.style.display = 'block';

    canvas.onmousedown = function(e) {
    // При нажатии мыши запоминаем, что мы начали рисовать и координаты.
    drawing = true;
    coordinates.push([e.offsetX, e.offsetY]);
};

    canvas.onmousemove = function(e) {
    // При движении мыши запоминаем координаты и рисуем линию.
    if (drawing) {
    var last = coordinates[coordinates.length - 1];
    ctx2d.beginPath();
    ctx2d.moveTo(last[0], last[1]);
    ctx2d.lineTo(e.offsetX, e.offsetY);
    ctx2d.stroke();

    coordinates.push([e.offsetX, e.offsetY]);
}
};

    return new Promise(function(resolve) {
    // При отпускании мыши запоминаем координаты и скрываем канвас.
    canvas.onmouseup = function(e) {
    coordinates.push([e.offsetX, e.offsetY]);
    canvas.style.display = 'none';
    drawing = false;

    coordinates = coordinates.map(function(x) {
    return [x[0] / canvas.width, x[1] / canvas.height];
});

    resolve(coordinates);
};
});
}

    // Метод IPolygonGeometryAccess
    let myPolygon = new ymaps.geometry.Polygon([
    [
    [57.14, 65.55],
    [68, 55],
    [86, 56]
    ]
    ]);

    // Метод работает только с корректно заданной картой.
    myPolygon.options.setParent(myMap.options);
    myPolygon.geometry.setMap(myMap);

    // Проверка, входит ли точка клика в полигон, с заданной выше геометрией.
    myMap.events.add('click', function(e) {
    alert(myPolygon.geometry.contains(e.get('#draw-canvas')) ? 'Успешно' : 'Еще раз');
});


