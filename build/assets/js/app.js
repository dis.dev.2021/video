let documentReady = function (fn) {
    if (document.readyState === 'loading') {
        document.addEventListener('DOMContentLoaded', fn);
    } else {
        fn();
    }
}

documentReady (() => {

    // START Mobile height fix
    const changeHeight = () => {
        let vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty('--vh', `${vh}px`);
    };

    changeHeight();

    window.addEventListener('resize', () => {
        changeHeight();
    });
    // END Mobile height fix

    $(".select2").select2({
        // the following code is used to disable x-scrollbar when click in select input and
        // take 100% width in responsive also
        dropdownAutoWidth: true,
        width: '100%'
    });

    $('.daterange').daterangepicker({
        locale: {
            format: 'MM/DD/YYYY',
            daysOfWeek: ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'],
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        },
        autoApply: true,
    });


    document.querySelectorAll('.pretty-scroll').forEach(el => {
        new PerfectScrollbar(el,{
            wheelSpeed: 1,
            wheelPropagation: true,
            minScrollbarLength: 20
        })
    });

    document.querySelectorAll('.pretty-scroll').forEach(el => {
        new PerfectScrollbar(el,{
            wheelSpeed: 1,
            wheelPropagation: true,
            minScrollbarLength: 20
        })
    });
});



