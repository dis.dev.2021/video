let sourceDir       = 'src';
let projectDir      = 'build';
let preprocessor    = 'scss';


let paths = {

    html: {
        src     : sourceDir + '/*.njk',
        dest    : projectDir + '/',
    },
    styles: {
        src     : sourceDir + '/assets/scss/**/*.scss',
        dest    : projectDir + '/assets/css/',
    },
    scripts: {
        src     : sourceDir + '/assets/js/*.js',
        dest    : projectDir + '/assets/js/',
    },
    images: {
        src     : [sourceDir + '/assets/img/**/*.{jpg,png,gif,ico,webp}'],
        dest    : projectDir + '/assets/img/',
    },
    svg: {
        src     : sourceDir + '/assets/img/**/*.svg',
        dest    : projectDir + '/assets/img/',
    },
    fonts: {
        src     : sourceDir + '/assets/fonts/**/*.*',
        dest    : projectDir + '/assets/fonts/',
    },
    lib: {
        js      : [
       
        ],
        css     : [
       
        ]
    },
    watch: {
        html    : sourceDir + '/**/*.njk',
        css     : sourceDir + '/assets/scss/**/*.scss',
        js      : sourceDir + '/assets/js/**/*.js',
        svg     : sourceDir + '/assets/img/**/*.svg',
        img     : sourceDir + '/assets/img/**/*.{jpg,png,gif,ico,webp}'
    },

    clean: './' +  projectDir + '/assets/'
}


const { src, dest, series, parallel } = require('gulp');
const gulp          = require('gulp');
const sass          = require('gulp-sass');
const cleanCSS      = require('gulp-clean-css');
const concat        = require('gulp-concat');
const rename        = require('gulp-rename');
const browsersync   = require('browser-sync');
const uglify        = require('gulp-uglify-es').default;
const autoprefixer  = require('gulp-autoprefixer');
const imagemin      = require('gulp-imagemin');
const newer         = require('gulp-newer');
const del           = require('del');
const sourcemaps    = require('gulp-sourcemaps');
const prettyHtml     = require('gulp-pretty-html');
const nunjucksRender = require('gulp-nunjucks-render');


function browserSync(params) {
    browsersync.init({
        server: {
            baseDir: './' +  projectDir + '/',
        },
        port: 3000,
        notify: false,
        online: true,
		open: false,
    })
}


function html() {
    return src(paths.html.src)
        .pipe(nunjucksRender())
        .pipe(prettyHtml({
            indent_size: 4,
            indent_char: ' ',
            unformatted: ['code', 'pre', 'em', 'strong', 'span', 'i', 'b', 'br']
        }))
        .pipe(dest(paths.html.dest))
        .pipe(browsersync.stream())
}

gulp.task('nunjucks', function() {
    return gulp.src(paths.html.src)
        .pipe(nunjucksRender())
        .pipe(gulp.dest(paths.html.dest));
});

function scripts() {
    return src(paths.scripts.src)
        .pipe(dest(paths.scripts.dest))
        .pipe(
            uglify()
        )
        .pipe(
            rename({
                extname: '.min.js'
            })
        )
        .pipe(dest(paths.scripts.dest))
        .pipe(browsersync.stream())
}

function styles() {
    return gulp.src(paths.styles.src)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(
            sass({
                outputStyle: 'expanded'
            })
        )
        .pipe(
            autoprefixer({
                overrideBrowserslist: ['last 5 versions'],
                cascade: true
            })
        )
        .pipe(dest(paths.styles.dest))
        .pipe(cleanCSS())
        .pipe(
            rename({
                extname: '.min.css'
            })
        )
        .pipe(sourcemaps.write())
        .pipe(dest(paths.styles.dest))
        .pipe(browsersync.stream())
}

function images() {
	return src(paths.images.src)
	.pipe(newer(paths.images.dest))
	.pipe(imagemin())
	.pipe(dest(paths.images.dest))
}

function svg() {
    return src(paths.svg.src)
    .pipe(newer(paths.svg.dest))
    .pipe(dest(paths.svg.dest))
}

function fonts() {
    return src(paths.fonts.src)
        .pipe(dest(paths.fonts.dest))
        .pipe(newer(paths.fonts.dest))
}

function libsJS() {
    return src(paths.lib.js)
        .pipe(concat('libs.js'))
        .pipe(dest(paths.scripts.dest))
        .pipe(
            uglify()
        )
        .pipe(
            rename({
                extname: '.min.js'
            })
        )
        .pipe(dest(paths.scripts.dest))
}

function libsCSS() {
    return src(paths.lib.css)
        .pipe(concat('libs.css'))
        .pipe(dest(paths.styles.dest))
        .pipe(cleanCSS())
        .pipe(
            rename({
                extname: '.min.css'
            })
        )
        .pipe(dest(paths.styles.dest))
}



function clean(params) {
    return del(paths.clean);
}

function watchFiles(params) {
    gulp.watch([paths.watch.html],html);
    gulp.watch([paths.watch.css],styles);
    gulp.watch([paths.watch.js],scripts);
    gulp.watch([paths.watch.img],images);
    gulp.watch([paths.watch.svg],svg);
}

exports.clean       = clean;
exports.html        = html;
exports.styles      = styles;
exports.scripts     = scripts;
exports.images      = images;
exports.svg         = svg;
exports.fonts       = fonts;
exports.libsJS      = libsJS;
exports.libsCSS     = libsCSS;
exports.build       = series(styles, scripts, images, html, fonts, svg);
exports.default     = parallel(watchFiles, browserSync);
exports.njk         = series(html);